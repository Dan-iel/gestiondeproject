<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gestion de Projets | Simplon.co</title>

    <link rel="stylesheet" href="style.css">
</head>

<body>
    <header>
        <div class="container">
            <div class="logo">
                <img src="https://simplonline-v3-prod.s3.eu-west-3.amazonaws.com/media/image/png/9ab5cd96-16ae-488d-803d-bf63779c5c2b.png" alt="Le logo du Simplon">
            </div>

            <div class="title">
                <h1>Gestion de Projets</h1>
            </div>
        </div>
    </header>
