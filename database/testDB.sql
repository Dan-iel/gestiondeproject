-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 16, 2021 at 01:42 PM
-- Server version: 8.0.27-0ubuntu0.20.04.1
-- PHP Version: 8.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gest`
--

-- --------------------------------------------------------

--
-- Table structure for table `Client`
--

CREATE TABLE `Client` (
  `id` int NOT NULL,
  `nom` varchar(45) NOT NULL,
  `adresse` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `Client`
--

INSERT INTO `Client` (`id`, `nom`, `adresse`) VALUES
(1, 'Raph', 'Simplon'),
(2, 'Orel', 'Simplon'),
(3, 'Max', 'Simplon');

-- --------------------------------------------------------

--
-- Table structure for table `Dev`
--

CREATE TABLE `Dev` (
  `id` int NOT NULL,
  `nom` varchar(45) NOT NULL,
  `prenom` varchar(45) NOT NULL,
  `niveau` varchar(45) NOT NULL COMMENT '			',
  `Projects_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='\n';

-- --------------------------------------------------------

--
-- Table structure for table `DevPro`
--

CREATE TABLE `DevPro` (
  `Dev_id` int NOT NULL,
  `Projects_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Projects`
--

CREATE TABLE `Projects` (
  `id` int NOT NULL,
  `demande` varchar(100) NOT NULL,
  `client_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `Projects`
--

INSERT INTO `Projects` (`id`, `demande`, `client_id`) VALUES
(1, 'Refonte du Site Web', 1),
(2, 'Site Web du PSG', 2),
(3, 'Application Mobile Tisséo', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Client`
--
ALTER TABLE `Client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Dev`
--
ALTER TABLE `Dev`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `DevPro`
--
ALTER TABLE `DevPro`
  ADD PRIMARY KEY (`Dev_id`,`Projects_id`),
  ADD KEY `fk_DevPro_2_idx` (`Projects_id`);

--
-- Indexes for table `Projects`
--
ALTER TABLE `Projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Projects_client_idx` (`client_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Client`
--
ALTER TABLE `Client`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `Dev`
--
ALTER TABLE `Dev`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Projects`
--
ALTER TABLE `Projects`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `DevPro`
--
ALTER TABLE `DevPro`
  ADD CONSTRAINT `fk_DevPro_dev` FOREIGN KEY (`Dev_id`) REFERENCES `Dev` (`id`),
  ADD CONSTRAINT `fk_DevPro_pro` FOREIGN KEY (`Projects_id`) REFERENCES `Projects` (`id`);

--
-- Constraints for table `Projects`
--
ALTER TABLE `Projects`
  ADD CONSTRAINT `fk_Projects_client` FOREIGN KEY (`client_id`) REFERENCES `Client` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
