<?#php require 'delprojet.php'; ?>

<a href="addprojet.php">Ajouter un projet</a>

<table>
   <thead colspa>
   <tr>
        <th>
            Project
        </th>

        <th></th>
    </tr>
   </thead>
   <tbody>

    <?php
    $sql = ('SELECT * FROM Projects');
    $req = $pdo->query($sql);
    $res = $req->fetchAll();
    
    foreach($res as $row){?>
        <tr>
            <td><?= $row['demande']; ?></td>
            <td> <a href="voirprojet.php?id=<?= $row['id'];?>" >Voir</a>  |   
            <a href="modprojet.php?id=<?= $row['id'];?>">Modifier</a>  |  
            <a href="delprojet.php?id=<?= $row['id'];?>" onclick="return confirm('Êtes vous sûr de vouloir supprimer la tâche: <?= $row['demande']; ?> ?')">Supprimer</a></td>
        </tr>
    <?php
    }
    ?>
   </tbody>
</table>